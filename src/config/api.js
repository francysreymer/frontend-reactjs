const URL = 'http://localhost:9000'

const API = {
    companies: {
        index: URL + '/companies',
        add: URL + '/companies/add',
        edit: URL + '/companies/edit/',
        read: URL + '/companies/read/',
        delete: URL + '/companies/delete/',
    },
    themes: {
        index: URL + '/themes',
        add: URL + '/themes/add',
        edit: URL + '/themes/edit/',
        read: URL + '/themes/read/',
        delete: URL + '/themes/delete/',
    },
    sites: {
        index: URL + '/sites',
        add: URL + '/sites/add',
        edit: URL + '/sites/edit/',
        read: URL + '/sites/read/',
        delete: URL + '/sites/delete/',
    },
    categories: {
        index: URL + '/categories',
        add: URL + '/categories/add',
        edit: URL + '/categories/edit/',
        read: URL + '/categories/read/',
        delete: URL + '/categories/delete/',
    },
    blogs: {
        index: URL + '/blogs',
        add: URL + '/blogs/add',
        edit: URL + '/blogs/edit/',
        read: URL + '/blogs/read/',
        delete: URL + '/blogs/delete/',
    },
    pages: {
        index: URL + '/pages',
        add: URL + '/pages/add',
        edit: URL + '/pages/edit/',
        read: URL + '/pages/read/',
        delete: URL + '/pages/delete/',
    },
    blocks: {
        index: URL + '/blocks',
        add: URL + '/blocks/add',
        edit: URL + '/blocks/edit/',
        read: URL + '/blocks/read/',
        delete: URL + '/blocks/delete/',
    },
    users: {
        index: URL + '/users',
        add: URL + '/users/add',
        edit: URL + '/users/edit/',
        read: URL + '/users/read/',
        delete: URL + '/users/delete/',
        login: URL + '/users/login/',
    }
}

export default API