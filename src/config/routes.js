import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import LayoutPage from '../components/Layout/Page';
import Layout from '../components/Layout';
import Login from '../pages/Login';

export default function Routes(){
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/companies" render={() => <Layout component="Company" />} />
                <Route path="/themes" render={() => <Layout component="Theme" />} />
                <Route path="/sites" render={() => <Layout component="Site" />} />
                <Route path="/categories" render={() => <Layout component="Category" />} />
                <Route path="/blogs" render={() => <Layout component="Blog" />} />
                <Route path="/site/pages/:id" render={() => <Layout component="LayoutPage" />} />
                <Route path="/users" render={() => <Layout component="User" />} />
                <Route path="/login" component={Login} />
            </Switch>        
        </BrowserRouter>
    );
}