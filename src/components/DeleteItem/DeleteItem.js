import React from 'react'; 
import './DeleteItem.scss';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { lighten, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    width: 600,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const DeleteItem = (props) => {

  const classes = useStyles();

  return (
  <Modal
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    className={classes.modal}
    open={props.modal}
    onClose={() => props.modalClose()}
    closeAfterTransition
    BackdropComponent={Backdrop}
    BackdropProps={{
      timeout: 500,
    }}
  >
    <Fade in={props.modal}>
      <div className={classes.paper}>
        <div className="title-group-delete">
          <div className="title">
            {props.title ? props.title : 'Excluir Item'}
          </div>
          <div className="title-close">
            <CloseIcon 
              onClick={() => props.modalClose()} 
              className="button-close" 
            />
          </div>
        </div>
        <div className="delete-content">
          <div className="delete-text">
           {props.deleteText ? props.deleteText : 'Você tem certeza que deseja excluir este item?'}
          </div>
          <div className="form-button-group">
            <div className="form-buttons">
              <div className="button-cancel">
                <button type="button" className="btn btn-secondary btn-sm btn-cancel" onClick={() => props.modalClose()}>
                  <CancelIcon color="inherit" fontSize="small" />  Cancelar
                </button>
              </div>
              <div className="button-delete">
                <button type="button" className="btn btn-danger btn-sm btn-save" onClick={() => props.handleDelete()}>
                  <DeleteIcon color="inherit" fontSize="small" />  Confirmar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fade>
  </Modal>
  );
}

export default DeleteItem;