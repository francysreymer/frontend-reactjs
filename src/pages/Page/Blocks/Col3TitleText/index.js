import React, { useState, useEffect } from 'react'; 
import EditorText from '../../../../components/TinyMCE';
import '../index.scss';

const Col3TitleText = (props) => {

  let initialState = {
    title_1: '',
    title_2: '',
    title_3: '',
    text_1: '',
    text_2: '',
    text_3: '',
  };

  if(props.content.fields) {
    initialState = {
      title_1: props.content.fields.title_1,
      title_2: props.content.fields.title_2,
      title_3: props.content.fields.title_3,
      text_1: props.content.fields.text_1,
      text_2: props.content.fields.text_2,
      text_3: props.content.fields.text_3,
    };
  }

  const [
    {
        title_1,
        title_2,
        title_3,
        text_1,
        text_2,
        text_3,
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  useEffect(function() {
    clearState()
  }, [props.content]);

  const buildContent = () => {

    const content = {
        fields: {
            title_1: title_1,
            title_2: title_2,
            title_3: title_3,
            text_1: text_1,
            text_2: text_2,
            text_3: text_3,
        },
        html: '<div class="Col3TitleText">'+
            '<div class="form-row">'+
            '<div class="form-group col-md-4">'+
            '<div class="title_1">'+title_1+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="title_2">'+title_2+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="title_3">'+title_3+'</div>'+
            '</div>'+
            '</div>'+
            '<div class="form-row">'+
            '<div class="form-group col-md-4">'+
            '<div class="text_1">'+text_1+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="text_2">'+text_2+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="text_3">'+text_3+'</div>'+
            '</div>'+
            '</div>'+
            '</div>',
    }
    props.changeBlock(content)
  };

  useEffect(() => { buildContent() }, [
    title_1,
    title_2,
    title_3,
    text_1,
    text_2,
    text_3,])

  const onChange = (content, name) => {
    setState(prevState => ({ ...prevState, [name]: content }));
  };

  return (
    <form className="form">
        <div className="form-row">
            <div className="form-group col-md-4">
                <div className="title-block">Título 1:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_1')} initialValue={title_1} type="title" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Título 2:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_2')} initialValue={title_2} type="title" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Título 3:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_3')} initialValue={title_3} type="title" />
            </div>
        </div>
        <div className="form-row">
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 1:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_1')} initialValue={text_1} type="text" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 2:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_2')} initialValue={text_2} type="text" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 3:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_3')} initialValue={text_3} type="text" />
            </div>
        </div>
    </form>
  );
}

export default Col3TitleText;