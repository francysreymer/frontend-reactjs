import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import axios from 'axios';
import API from '../../config/api';
import './index.scss';

export default function Login() {

  const initialState = {
    username: '',
    password: '',
  };

  const [
    {
      username,
      password,
    },
    setState
  ] = useState(initialState);

  const [user, setUser] = useState('');
  const [error, setError] = useState(false);

  const onChangeUsername = e => {
    setState(prevState => ({ ...prevState, 'username': e.target.value }));
  };

  const onChangePassword = e => {
    setState(prevState => ({ ...prevState, 'password': e.target.value }));
  };

  useEffect(function() {
    localStorage.setItem("user", null);
  }, []);

  const handleLogin = () => {
    setError(false)  
    
    const form = {
      username: username,
      password: password,
    }

    async function login() {
      try {
        const response = await axios.post(API.users.login, form);
        localStorage.setItem("user", JSON.stringify(response.data));
        setUser(JSON.stringify(response.data))  
      } catch(error) {
        setUser('')  
        setError(true)  
        localStorage.setItem("user", null);
      }
    }
    login();
  }

  return (
  <>
  {!user ?
    <>
      <section className="form-login animated flipInX">
        <h2>JustSites</h2>
        {error && <p className="error">Usuário ou senha incorreto!</p>}
        <form className="loginbox" autoComplete="off">
          <input placeholder="Usuário" type="text" id="username" value={username} onChange={onChangeUsername}></input>
          <input placeholder="Senha" type="password" id="password" value={password} onChange={onChangePassword}></input>
          <button type="button" className="btn btn-primary btn-sm buttonSave" onClick={handleLogin}>
            Login
          </button>
        </form>
      </section>
    </>
      : <Redirect to="/companies" />
  }
  </>
  );
}
